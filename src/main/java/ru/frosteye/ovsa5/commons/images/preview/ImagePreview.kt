package ru.frosteye.ovsa5.commons.images.preview

import android.graphics.Bitmap
import java.io.File

sealed class ImagePreview {

    data class Remote(
        val url: String
    ) : ImagePreview()

    data class Local(
        val file: File
    ) : ImagePreview()

    data class Raw(
        val bitmap: Bitmap
    ) : ImagePreview()

    data class Resource(
        val drawableRes: Int
    ) : ImagePreview()
}

fun String?.asImagePreview(): ImagePreview.Remote? {
    if (this == null) return null
    return ImagePreview.Remote(this)
}

fun File?.asImagePreview(): ImagePreview.Local? {
    if (this == null) return null
    return ImagePreview.Local(this)
}

fun Bitmap?.asImagePreview(): ImagePreview.Raw? {
    if (this == null) return null
    return ImagePreview.Raw(this)
}