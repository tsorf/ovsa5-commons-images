package ru.frosteye.ovsa5.commons.images.loader

import android.widget.ImageView
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

interface ImageLoader {

    fun loadImage(image: ImagePreview?): Builder

    interface Builder {

        fun centerCrop(): Builder

        fun alternative(alternativePreview: ImagePreview): Builder

        fun into(imageView: ImageView)
    }
}